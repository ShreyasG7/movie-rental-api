const express = require('express');

const movies = require('./src/controllers/movieRoutes');
const directors = require('./src/controllers/directorRoutes');
const logger = require('./src/middlewares/winstonLogger');
const errorHandlers = require('./src/middlewares/errorHandler');

const app = express();

app.use(logger);
app.use('/api/movies', movies);
app.use('/api/directors', directors);
app.use(errorHandlers.initialErrorHandler);
app.use(errorHandlers.finalErrorHandler);
const port = process.env.port || 3000;

app.listen(port, () => console.log(`Listening on port ${port}`));
