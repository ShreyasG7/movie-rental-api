const Joi = require('@hapi/joi');

const directorIdValidation = requestIdBody => {
  const directorIdSchema = Joi.object({
    directorId: Joi.number()
  });
  return directorIdSchema.validate(requestIdBody);
};

const directorNameValidation = requestNameObject => {
  const directorNameSchema = Joi.object({
    directorName: Joi.string()
      .min(3)
      .required()
  });
  return directorNameSchema.validate(requestNameObject);
};

module.exports = {
  directorIdValidation,
  directorNameValidation
};
