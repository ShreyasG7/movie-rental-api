const Joi = require('@hapi/joi');

const movieIdValidation = requestIdObject => {
  const movieIdSchema = Joi.object({
    movieId: Joi.number()
  });
  return movieIdSchema.validate(requestIdObject);
};

const movieNameAndIdValidation = requestMovieObject => {
  const movieNameAndIdSchema = Joi.object({
    directorId: Joi.number().required(),
    movieName: Joi.string().required()
  });
  return movieNameAndIdSchema.validate(requestMovieObject);
};

module.exports = {
  movieIdValidation,
  movieNameAndIdValidation
};
