const winstonLogger = require('../config/logger');

const logger = (req, res, next) => {
  winstonLogger.info(`${req.method}: ${req.originalUrl}`);
  next();
};

module.exports = logger;
