const { Pool } = require('pg');
require('dotenv').config();

const pool = new Pool({
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  host: process.env.DB_HOST,
  post: 5432,
  database: process.env.DB_DATABASE,
  max: 10,
  idleTimeoutMillis: 1
});

const getSpecificMovie = async movieId => {
  try {
    const client = await pool.connect();
    const specificMovie = await client.query(
      'select * from movies where movie_id = $1',
      [movieId]
    );
    client.release();
    return specificMovie;
  } catch (error) {
    console.error(error);
  }
};

const getAllMovies = async () => {
  try {
    const client = await pool.connect();
    const allMovies = await client.query(
      'select * from movies order by movie_id asc'
    );
    client.release();
    return allMovies;
  } catch (error) {
    console.error(error);
  }
};

const insertSpecificMovie = async (directorId, movieName) => {
  try {
    const client = await pool.connect();
    await client.query(
      'insert into movies (director_id, movie_name) values ($1, $2)',
      [directorId, movieName]
    );
    client.release();
  } catch (error) {
    console.error(error);
  }
};

const updateSpecificMovie = async (movieName, directorId) => {
  try {
    const client = await pool.connect();
    await client.query(
      'update movies set movie_name = $1 where director_id = $2',
      [movieName, directorId]
    );
    client.release();
  } catch (error) {
    console.error(error);
  }
};

const deleteSpecificMovie = async movieId => {
  try {
    const client = await pool.connect();
    await client.query('delete from movies where movie_id = $1', [movieId]);
    client.release();
  } catch (error) {
    console.error(error);
  }
};

const movieExists = async movieId => {
  try {
    const specificMovie = await getSpecificMovie(movieId);
    return !specificMovie.rows.length;
  } catch (error) {
    console.error(error);
  }
};

module.exports = {
  getSpecificMovie,
  getAllMovies,
  insertSpecificMovie,
  updateSpecificMovie,
  deleteSpecificMovie,
  movieExists
};
