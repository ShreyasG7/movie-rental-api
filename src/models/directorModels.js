const { Pool } = require('pg');
require('dotenv').config();

const pool = new Pool({
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  host: process.env.DB_HOST,
  post: 5432,
  database: process.env.DB_DATABASE,
  max: 10,
  idleTimeoutMillis: 1
});

const getSpecificDirector = async directorId => {
  try {
    const client = await pool.connect();
    const specificDirector = await client.query(
      'select * from directors where id = $1',
      [directorId]
    );
    client.release();
    return specificDirector;
  } catch (error) {
    console.error(error);
  }
};

const getAllDirectors = async () => {
  try {
    const client = await pool.connect();
    const allDirectors = await client.query('select * from directors');
    client.release();
    return allDirectors;
  } catch (error) {
    console.error(error);
  }
};

const insertSpecificDirector = async directorName => {
  try {
    const client = await pool.connect();
    await client.query('insert into directors (director_name) values ($1)', [
      directorName
    ]);
    client.release();
  } catch (error) {
    console.error(error);
  }
};

const updateSpecificDirector = async (directorName, directorId) => {
  try {
    const client = await pool.connect();
    await client.query(
      'update directors set director_name = $1 where id = $2',
      [directorName, directorId]
    );
    client.release();
  } catch (error) {
    console.error(error);
  }
};

const deleteSpecificDirector = async directorId => {
  try {
    const client = await pool.connect();
    await client.query('delete from directors where id = $1', [directorId]);
    client.release();
  } catch (error) {
    console.error(error);
  }
};

const directorExists = async directorId => {
  try {
    const specificDirector = await getSpecificDirector(directorId);
    return !specificDirector.rows.length;
  } catch (error) {
    console.error(error);
  }
};

module.exports = {
  getSpecificDirector,
  getAllDirectors,
  insertSpecificDirector,
  updateSpecificDirector,
  deleteSpecificDirector,
  directorExists
};
