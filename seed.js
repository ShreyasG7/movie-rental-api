const { Pool } = require('pg');
const fs = require('fs');
require('dotenv').config();

const pool = new Pool({
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  host: process.env.DB_HOST,
  post: 5432,
  database: process.env.DB_DATABASE,
  max: 10,
  idleTimeoutMillis: 1
});

// Reading seed data and returning it in form of object
const moviesJsonToMoviesDB = () => {
  const moviesData = fs.readFileSync('seedData.json', 'UTF-8');
  return JSON.parse(moviesData);
};

// Check if directors table is empty or not
const checkIfEmptyTableDirectors = async () => {
  const client = await pool.connect();
  const query = await client.query('select * from directors');
  client.release();
  return query.rows.length === 0;
};

// Check if movies table is empty or not
const checkIfEmptyTableMovies = async () => {
  const client = await pool.connect();
  const query = await client.query('select * from movies');
  client.release();
  return query.rows.length === 0;
};

// Add list of directors to table if empty otherwise do not
const addDirectorsTable = async seedData => {
  const uniqueDirectorNames = seedData.reduce(
    (uniqueDirectorNamesAccum, eachMovieData) => {
      uniqueDirectorNamesAccum[eachMovieData.Director] = true;
      return uniqueDirectorNamesAccum;
    },
    {}
  );
  if (await checkIfEmptyTableDirectors()) {
    Object.keys(uniqueDirectorNames).forEach(async director => {
      const client = await pool.connect();
      await client.query(`insert into directors (director_name) values ($1);`, [
        director
      ]);
      client.release();
    });
  }
};

// Add list of movies to table if empty otherwise do not
const addMoviesTable = async seedData => {
  if (await checkIfEmptyTableMovies()) {
    seedData.forEach(async movieDetails => {
      const clientPool = await Promise.all(
        Array.from({ length: 2 }, (_, i) => i).map(num => pool.connect())
      );
      const directorId = await clientPool[0].query(
        'select id from directors where director_name = $1;',
        [movieDetails.Director]
      );
      await clientPool[1].query(
        `insert into movies (director_id, movie_name) values ($1, $2);`,
        [directorId.rows[0].id, movieDetails.Title]
      );
      clientPool.forEach(client => client.release());
    });
  }
};

addMoviesTable(moviesJsonToMoviesDB());
addDirectorsTable(moviesJsonToMoviesDB());
